package textGenerator;

import java.util.ArrayList;
import java.util.Random;

public class TextGenerator {

    public static final int NUMBER_OF_LETTERS = 32;
    public static final int BEGIN_FROM_INDEX = 192;
    public static final int LETTERS_IN_WORD_COEF = 4;
    public static final int WORDS_IN_SENTENCE_COEF = 4;
    public static final int SENTENCES_IN_LINE_COEF = 4;
    public static final int FOR_BETTER_RAND_COEF = 1000;
    public static final String CURRENT_CHARSET = "ISO-8859-1";
    public static final String WANTED_CHARSET = "KOI8_R";
    public static final String CONSONANTS = "бвгджзйклмнпрстфчцчшщ";
    public static final String VOWELS = "аеиоуюя";
    public static final int[] FOR_BETTER_RAND_OF_MORPHEMS_COEF = {3, 5};

    public static void main(String[] args) {
        new TextGenerator().run(Integer.parseInt(args[0]));
    }

    /**
     * main logical method of program, prints random generated text to console
     *
     * @param numberOfSentences in text
     */
    public void run(int numberOfSentences) {
        System.out.println(getText(numberOfSentences));
    }

    /**
     * get the generated text
     *
     * @param numberOfSentences in text
     * @return String with generated text
     */
    private String getText(int numberOfSentences) {
        String text = "";
        for (int i = 0; i < numberOfSentences; i++) {
            int coefOfSentencesInLine = getRandomNumber(SENTENCES_IN_LINE_COEF) + 1;
            boolean isEndOfSentence = (i == numberOfSentences - 1);
            boolean isNumberOfSentencesInLineOk = (i % coefOfSentencesInLine != 1);
            text += isEndOfSentence || isNumberOfSentencesInLineOk ? getSentence() + " " : getSentence() + "\n";
        }
        return text;
    }

    /**
     * generate the sentence from words
     *
     * @return generated sentence
     */
    private String getSentence() {
        String sentence = "";
        int numberOfWords = getRandomNumber(WORDS_IN_SENTENCE_COEF) + 1;
        for (int i = 0; i < numberOfWords; i++) {
            String word = getWord();
            char firstLetterInSentence = Character.toUpperCase(word.charAt(0));
            String restOfWord = word.substring(1);
            boolean isBeginOfSentence = i == 0;
            boolean isEndOfSentence = i == numberOfWords - 1;
            sentence += isBeginOfSentence && isEndOfSentence
                    ? firstLetterInSentence + restOfWord
                    : isBeginOfSentence && !isEndOfSentence
                            ? firstLetterInSentence + restOfWord + " "
                            : isEndOfSentence
                                    ? word : word + " ";
        }
        return sentence + ".";
    }

    /**
     * method generate random words
     *
     * @return random generated word
     */
    private String getWord() {
        return (getRandomNumber(FOR_BETTER_RAND_COEF) % FOR_BETTER_RAND_OF_MORPHEMS_COEF[0] == 1)
                ? getRoot()
                : (getRandomNumber(FOR_BETTER_RAND_COEF) % FOR_BETTER_RAND_OF_MORPHEMS_COEF[1] == 1)
                ? getPrefix() + getRoot() : getPrefix() + getRoot() + getPostfix();
    }

    /**
     * method return random prefix for word
     *
     * @return String random prefix for word
     */
    private String getPrefix() {
        ArrayList<String> prefix = new ArrayList<>();
        prefix.add("пре");
        prefix.add("при");
        prefix.add("пост");
        prefix.add("суб");
        prefix.add("гипер");
        prefix.add("гипо");
        prefix.add("из");
        prefix.add("за");
        prefix.add("до");
        prefix.add("под");
        prefix.add("пра");
        prefix.add("пред");
        prefix.add("при");
        prefix.add("про");
        prefix.add("раз");
        prefix.add("с");
        prefix.add("к");
        prefix.add("на");
        prefix.add("над");
        prefix.add("");
        return prefix.get(getRandomNumber(prefix.size()));
    }

    /**
     * method return random postfix for word
     *
     * @return String random postfix for word
     */
    private String getPostfix() {
        ArrayList<String> postfix = new ArrayList<>();
        postfix.add("сь");
        postfix.add("ся");
        postfix.add("то");
        postfix.add("либо");
        postfix.add("нибудь");
        postfix.add("таки");
        postfix.add("ок");
        postfix.add("ая");
        postfix.add("ый");
        postfix.add("ое");
        postfix.add("ой");
        postfix.add("ия");
        postfix.add("ие");
        postfix.add("ии");
        postfix.add("ем");
        postfix.add("");
        return postfix.get(getRandomNumber(postfix.size()));
    }

    /**
     * generate root of word, return char sequence from one of two variants of
     * generating with 50% of randomness
     *
     * @return random chars generated string
     */
    private String getRoot() {
        return Math.random() > 0.5 ? getRandomCharSequence() : getRandomCharSequenceSecVar();
    }

    /**
     * get random char sequence second variant
     *
     * @return random char sequence
     */
    private String getRandomCharSequenceSecVar() {
        String root = "";
        int numberOfLetters = getRandomNumber(LETTERS_IN_WORD_COEF) + 1;
        for (int i = 0; i < numberOfLetters; i++) {
            root += VOWELS.charAt(getRandomNumber(VOWELS.length()));
            root += (i % 2 == 1 || i == 0) && numberOfLetters > 1 ? CONSONANTS.charAt(getRandomNumber(CONSONANTS.length())) : "";
        }
        return root;
    }

    /**
     * get random char sequence first variant
     *
     * @return random char sequence
     */
    private String getRandomCharSequence() {
        String randomCharSequence = "";
        for (int i = 0; i < getRandomNumber(LETTERS_IN_WORD_COEF) + 1; i++) {
            randomCharSequence += getRandomChar('v') + getRandomChar('c');
        }
        return randomCharSequence;
    }

    /**
     * get random letter
     *
     * @param vowelOrConsonant must print here c(for consonant) or v(for vowel)
     * @return vowel or consonant letter
     */
    private String getRandomChar(char vowelOrConsonant) {
        String letter = "";
        try {
            letter = new String(((char) (getRandomNumber(FOR_BETTER_RAND_COEF) % NUMBER_OF_LETTERS + BEGIN_FROM_INDEX) + "").getBytes(CURRENT_CHARSET), WANTED_CHARSET);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (vowelOrConsonant == 'c' && CONSONANTS.contains(letter)) {
            return letter;
        } else if (vowelOrConsonant == 'v' && VOWELS.contains(letter)) {
            return letter;
        } else {
            return getRandomChar(vowelOrConsonant);
        }
    }

    /**
     * get random generated integer number with max limit value
     *
     * @param limitValue
     * @return random integer number
     */
    private int getRandomNumber(int limitValue) {
        return new Random().nextInt(limitValue);
    }
}
